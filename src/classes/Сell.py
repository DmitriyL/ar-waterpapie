
class Cell:
    shape = 0

    def __init__(self):
        self.shape = 0

    def set_shape(self, new_shape: int):
        self.shape = new_shape

    def get_shape(self):
        return self.shape
