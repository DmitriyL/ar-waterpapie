from random import randint
from src.classes.Сell import Cell


#  Представление матрицы 1 координата это этаж 2 координата подъезд

class Field:
    field = list(list())
    start = []
    stop = []

    def __init__(self):
        for i in range(0, 8):
            cc = []
            for j in range(0, 8):
                c = Cell()
                cc.append(c)
            self.field.append(cc)
        self.put_markers()

    def put_markers(self):
        a = randint(1, 6)
        b = randint(1, 6)
        self.field[1][a].set_shape(3)
        self.start = [1, a]
        self.stop = [6, b]
        self.field[6][b].set_shape(3)

    def get_field(self):
        for i in range(1, 7):
            for j in range(1, 7):
                print(self.field[i][j].get_shape(), end=' ')
            print('')

    def get_cell(self, i, j):
        return self.field[i][j]

    def get_road(self):
        pass

    def is_road(self):
        data = self.field
        i = self.start[0] + 1
        j = self.start[1]
        return dfs(i, j, data)

    def create_stone(self, n):
        while n>0:
            a = randint(1, 6)
            b = randint(1, 6)
            if a != self.start[0] and b != self.start[1] and a != self.stop[0] and b != self.stop[1] and self.field[a][b].get_shape() != 4:
                self.field[a][b].set_shape(4)
                n -= 1


def dfs(i, j, data):
    a, b, c = 0, 0, 0
    if data[i][j].get_shape() == 3:
        return 1
    elif data[i][j].get_shape() == 1:
        data[i][j].set_shape(-1)
        if i + 1 < 7:
            a = dfs(i + 1, j, data)
        if j + 1 < 7:
            b = dfs(i, j + 1, data)
        if j - 1 > 0:
            c = dfs(i, j - 1, data)
        return a + b + c
    else:
        return 0
