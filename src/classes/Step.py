from src.classes.Field import Field


class Step:
    field = Field()

    def __init__(self, field: Field):
        self.field = field

    def _update_field_(self, new_field):
        self.field = new_field
